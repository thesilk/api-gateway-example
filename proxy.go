package main

import (
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/gorilla/mux"
)

var loginToken string

func handler(p *httputil.ReverseProxy, h http.Handler) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Token")
		if token == loginToken {
			r.URL.Path = mux.Vars(r)["rest"]
			p.ServeHTTP(w, r)
			return
		}
		h.ServeHTTP(w, r)

	}
}

func unauthorizedHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte("unauthorized access"))
}

func login(w http.ResponseWriter, r *http.Request) {

	loginToken = "123token123"

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("{'token': '123token123'}"))
}

func logout(w http.ResponseWriter, r *http.Request) {

	loginToken = ""

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("logout successful"))
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	target1 := "http://localhost:7001"
	remote1, err := url.Parse(target1)
	if err != nil {
		panic(err)
	}

	target2 := "http://localhost:7002"
	remote2, err := url.Parse(target2)
	if err != nil {
		panic(err)
	}

	proxy1 := httputil.NewSingleHostReverseProxy(remote1)
	proxy2 := httputil.NewSingleHostReverseProxy(remote2)
	r := mux.NewRouter()
	r.HandleFunc("/server1/{rest:.*}", handler(proxy1, http.HandlerFunc(unauthorizedHandler)))
	r.HandleFunc("/server2/{rest:.*}", handler(proxy2, http.HandlerFunc(unauthorizedHandler)))
	r.HandleFunc("/api/v0/login", login)
	r.HandleFunc("/api/v0/logout", logout)
	http.Handle("/", r)
	http.ListenAndServe(":3002", r)
}
