# API GATEWAY EXAMPLE

This is only a working proof how the authentication can work in a gateway api.

### Test it

```bash
go run server.go
```

creates two api servers.

```bash
go run proxy.go
```

starts the proxy.


#### login

```bash
curl http://localhost:3002/api/v0/login
```
copy token in header for the new request to communicate with the servers

```bash
curl -H "Token: 123token123" http://localhost:3002/server1/api/v0/server1
curl -H "Token: 123token123" http://localhost:3002/server2/api/v0/server2
```

#### logout

```bash
curl http://localhost:3002/api/v0/logout
```

### TODO
- using JWT for authentication
