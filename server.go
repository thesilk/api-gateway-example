package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func server(name string, port int, done chan bool) {
	router := mux.NewRouter()
	router.HandleFunc("/api/v0/"+name, func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(name))
	})
	log.Printf("starting %s on port %d\n", name, port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), router)
}

func main() {
	done := make(chan bool, 1)

	go server("server1", 7001, done)
	go server("server2", 7002, done)
	<-done
}
